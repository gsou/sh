(in-package :sh)


;;;; Function wrapper

(defun shell (cmd &key input (output :string))
  "Wrapper over `uiop:run-program'.
The CMD is directly passed to `run-program'. The additional option
INPUT can provide a raw string input, filepath or byte array. OUTPUT
can be either `:string' (the default) or `:binary' in which case the
output is considered to be bytes instead of a valid utf-8 string.
"
  (declare (type (member :string :binary) output)
           (type (or null string pathname (simple-array (unsigned-byte 8) (*))) input)
           (type string cmd))

  (let* ((input-stream
           (etypecase input
             ((simple-array (unsigned-byte 8) (*))
              (make-string-input-stream (babel:octets-to-string input :encoding :latin1)))
             (string (make-string-input-stream input))
             (stream input)
             (pathname input)
             (null nil)))
         (output-value (uiop:run-program cmd :input input-stream :output :string
                                             :external-format (if (eq output :binary) :latin1 :utf-8))))
    (if (eq output :binary) (babel:string-to-octets output-value :encoding :latin1) output-value)))
(defun shellb (cmd &optional input) (shell cmd :input input :output :binary))


;;;; Close enough shell parser

(defun read-quote-escape (stream args)
  (loop for c = (read-char stream t nil)
        until (char= c #\') appending
                            (cond
                              ((char= c #\\) (list (read-char stream t nil)))
                              (t (list c)))))
(defun read-dquote-escape (stream args)
  (loop for c = (read-char stream t nil)
        until (char= c #\") appending
                            (cond
                              ((char= c #\#)
                               ;; Lisp escape
                               (vector-push-extend (read stream t nil t) args)
                               (list t))
                              ((char= c #\\) (list (read-char stream t nil)))
                              (t (list c)))))
(defun read-shell-normal (stream stop args &aux escape done)
  "Read a single 'argument' from the stream."

  (let ((string (apply 'concatenate 'string
                       (mapcar
                        (lambda (x) (cond ((eq t x) (list #\~ #\A)) ((eq #\~ x) (list #\~ #\~)) (t (list x))))
                        (loop for c = (read-char stream t nil t)
                              until (or (sb-unicode:whitespace-p c) (when (char= c stop) (setq done t) t))
                              appending (cond
                                          ((char= c #\#)
                                           ;; Lisp escape
                                           (vector-push-extend `(uiop:escape-command (list (format nil "~A" ,(read stream t nil t)))) args)
                                           (list t))
                                          ((char= c #\") (setq escape t) (read-dquote-escape stream args))
                                          ((char= c #\') (setq escape t) (read-quote-escape stream args))
                                          (t (list c))))))))
    (values (if escape (uiop:escape-command (list string)) string) done)))
(defun read-case (stream &optional (eof-error-p t) eof-value recursive-p)
  (let* ((*readtable* (copy-readtable)))
    (setf (readtable-case *readtable*) :preserve)
    (read stream eof-error-p eof-value recursive-p)))
(defun read-shell-command (stream char &aux (args (make-array 0 :fill-pointer 0)) options)
  "Read a shell command from the STREAM to a string and complete lisp expansions #()."
  (declare (ignore char))
  (let* ((c (progn (peek-char t stream) (read-char stream t nil t))))
    (when (eq c #\$)
      (setq options (list :output :binary))
      (peek-char t stream)
      (setq c (read-char stream t nil t)))
    (when (eq c #\<)
      (setq options (nconc (list :input (read stream t nil t)) options))
      (peek-char t stream)
      (setq c (read-char stream t nil t)))
    (if (eq c #\()
        (progn
          (let ((cmd (format nil "~{~A~^ ~}"
                             (prog (acc)
                              again
                                (multiple-value-bind (next done) (read-shell-normal stream #\) args)
                                  (if (string= "!" next)
                                      (push " | " acc)
                                      (push next acc))
                                  (if done (return (reverse acc)) (go again)))))))
            `(shell (format nil ,cmd ,@(coerce args 'list)) ,@options)))
        `(uiop:getenv ,(concatenate 'string (string c) (symbol-name (read-case stream t nil t)))))))


;;;; Reader macro

(defun install-shell-reader (&optional (*readtable* *readtable*))
  (set-macro-character #\$ #'read-shell-command t))
