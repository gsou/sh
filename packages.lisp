
(defpackage :sh
  (:use :cl)
  (:export
   #:shell
   #:shellb
   #:install-shell-reader
   #:read-case))
