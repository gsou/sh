(defsystem "sh"
  :description "Simple macro and reader macro wrapping UIOP:RUN-PROGRAM to provide a simple way to call programs from lisp code."
  :version "0.1"
  :depends-on (:babel)
  :author "gsou"
  :license "BSD-2-Clause"
  :components
  ((:file "packages")
   (:file "sh" :depends-on ("packages"))))
